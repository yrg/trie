open Monolith

module R = Reference

module C = Candidate

let check model =
  C.check model, constant "check"

let trie =
  declare_abstract_type ~check ()

let arbitrary_key =
  Gen.(string (lt 10) char)

let extreme_key () =
  if Gen.bool () then "" else arbitrary_key ()

(* [present_key m] produces a key that is present in the map [m]. Its
   implementation is not efficient, but we will likely be working with
   very small maps, so this should be acceptable. *)

let present_key (m : R.trie) () =
  let n = R.cardinal m in
  let i = Gen.int n () in
  let key, _value = List.nth (R.bindings m) i in
  key

(* [key m] combines the above generators. *)

let key m () =
  if Gen.bool () then
    arbitrary_key ()
  else if Gen.bool () then
    extreme_key ()
  else
    present_key m ()

let key m =
  constructible @@ fun () ->
  let s = key m () in
  (s, constant (Printf.sprintf "\"%s\"" (String.escaped s)))

let random_key =
  constructible @@ fun () ->
  let s = arbitrary_key () in
  (s, constant (Printf.sprintf "\"%s\"" (String.escaped s)))

let value = lt 16

let reference_from_sorted_bindings x =
  x

let () =
  let spec = trie in
  declare "empty" spec R.empty C.empty;

  let spec = rot3 (trie ^>> fun m -> (key m ^> value ^> trie)) in
  declare "add" spec R.add C.add;

  let spec = rot2 (trie ^>> fun m -> (key m ^> trie)) in
  declare "remove" spec R.remove C.remove;

  let spec = rot2 (trie ^>> fun m -> (key m ^> option value)) in
  declare "find" spec R.find_opt C.find;

  let spec = (trie ^> int) in
  declare "cardinal" spec R.cardinal C.cardinal;

  let spec = (trie ^>> fun m -> list (key m *** int)) in
  declare "bindings" spec R.bindings C.bindings

  (* let spec = (trie ^> trie) in
   * declare "from_sorted_bindings" spec reference_from_sorted_bindings @@
   * fun t -> C.from_sorted_bindings (Array.of_list (C.bindings t)) *)

let () =
  let fuel = 5 in
  main fuel
