module Suffix = struct
  type t = { ofs : int; base : string }
  let of_string_suffix ofs base = { ofs; base }
  let of_string base = { ofs = 0; base }
  let to_string { ofs; base } = String.(sub base ofs (length base - ofs))
  let base { base; _ } = base
  let out i s = i >= String.length s.base
  let equal s1 s2 =
    let rec aux i j =
      let out1 = out i s1 and out2 = out j s2 in
      if out1 && out2 then true
      else if out1 || out2 || s1.base.[i] <> s2.base.[j] then false
      else aux (i + 1) (j + 1)
    in
    aux s1.ofs s2.ofs
  let destruct ({ ofs; base } as s) =
    if out ofs s then None else Some (s.base.[ofs], { ofs = ofs + 1; base })
  let is_empty { ofs : int; base : string } =
    ofs = String.length base

end

module CharMap = struct
  include Map.Make (Char)
end

type 'a t =
  | Empty
  | Node of (string * 'a) option * 'a t CharMap.t
  | Leaf of Suffix.t * 'a

let rec show print = function
  | Empty -> "empty"
  | Leaf (k, v) -> Printf.sprintf "leaf (%s, %s)" (Suffix.to_string k |> String.escaped) (print v)
  | Node (mv, cmap) ->
    Printf.sprintf "node (%s, %s)"
      (match mv with
       | None -> "none"
       | Some (s, v) -> Printf.sprintf "%s -> %s" s (print v))
      (show_cmap print cmap)

and show_cmap print m =
  (CharMap.bindings m |> List.map @@ fun (c, t) ->
  Printf.sprintf "%c -> %s" c (show print t))
  |> String.concat "; "

let empty = Empty

let add s v t =
  let rec aux k v = function
    | Empty ->
      Leaf (k, v)
    | Leaf (k', v') ->
      if Suffix.equal k k' then
        Leaf (k', v)
      else (
        let t1 = aux k' v' (Node (None, CharMap.empty)) in
        let t2 = aux k v t1 in
        (* Printf.printf "DEBUG:\n%s\n%s\n%s\n"
         *   (show string_of_int t)
         *   (show string_of_int t1)
         *   (show string_of_int t2)
         * ; *)
        t2
      )
    | Node (mv', cmap) ->
      match Suffix.destruct k with
      | None ->
        Node (Some (Suffix.base k, v), cmap)
      | Some (c, k) ->
        let cmap = CharMap.update c (function
            | None -> Some (Leaf (k, v))
            | Some t -> Some (aux k v t)
          ) cmap
        in
        Node (mv', cmap)
  in
  aux (Suffix.of_string s) v t

let find k t =
  let rec aux k = function
    | Empty -> None
    | Leaf (k', v') ->
      if Suffix.equal k k' then
        Some v'
      else
        None
    | Node (mv, cmap) ->
      match Suffix.destruct k with
      | None ->
        Option.map snd mv
      | Some (c, k) ->
        try
          aux k (CharMap.find c cmap)
        with Not_found -> None
  in
  aux (Suffix.of_string k) t

let remove k t =
  let rec aux k = function
    | Empty -> Empty
    | (Leaf (k', _) as t) ->
      if Suffix.equal k k' then
        Empty
      else
        t
    | (Node (mv, cmap) as t) ->
      match Suffix.destruct k with
      | None ->
        (match mv with None -> t | Some _ -> Node (None, cmap))
      | Some (c, k) ->
        let cmap' =
          CharMap.update c (function
              | None -> None
              | Some t -> Some (aux k t)) cmap
        in
        if cmap' == cmap then t else Node (mv, cmap')
  in
  aux (Suffix.of_string k) t

let fold (type a b) (f : string -> a -> b -> b) (t : a t) (init : b) =
  let rec aux accu = function
    | Empty -> accu
    | Leaf (k, v) -> f (Suffix.base k) v accu
    | Node (mv, cmap) ->
      let accu =
        match mv with
        | None -> accu
        | Some (s, v) -> f s v accu
      in
      CharMap.fold (fun _ t accu -> aux accu t) cmap accu
  in
  aux init t

let iter (type a) (f : string -> a -> unit) (t : a t) : unit =
  fold (fun k v () -> f k v) t ()

let cardinal t =
  fold (fun _ _ accu -> accu + 1) t 0

let bindings t =
  fold (fun k v accu -> (k, v) :: accu) t [] |> List.rev

let of_seq seq =
  Seq.fold_left (fun t (k, v) -> add k v t) empty seq

let map f t =
  let rec aux = function
    | Empty -> Empty
    | Leaf (k, v) -> Leaf (k, f v)
    | Node (mv, cmap) ->
       let mv = Option.map (fun (k, v) -> (k, f v)) mv in
       Node (mv, CharMap.map (fun t -> aux t) cmap)
  in
  aux t

let return x = Lwt.return (Ok x)

let ( >>= ) x f =
  Lwt.bind x @@ function
               | Ok v -> f v
               | e -> Lwt.return e

let seq_fold_es (type k a b e)
      (f : k -> a -> b -> (b, e) result Lwt.t) (s : (k * a) Seq.t) init
    : (b, e) result Lwt.t =
  let rec aux accu = function
    | Seq.Nil -> Lwt.return (Ok accu)
    | Seq.Cons ((k, v), s) -> f k v accu >>= fun accu -> aux accu (s ())
  in
  aux init (s ())

let fold_es (type a b e)
      (f : string -> a -> b -> (b, e) result Lwt.t) (t : a t) (init : b)
    : (b, e) result Lwt.t =
  let rec aux accu = function
    | Empty -> return accu
    | Leaf (k, v) -> f (Suffix.base k) v accu
    | Node (mv, cmap) ->
       (match mv with
        | None -> return accu
        | Some (s, v) -> f s v accu
       ) >>= fun accu ->
       CharMap.to_seq cmap |> fun s -> seq_fold_es (fun _ t accu -> aux accu t) s accu
  in
  aux init t

let from_sorted_bindings (bs : (string * 'a) array) =
  let invariant ofs start xstop =
    let ss = Array.(sub bs start (xstop - start) |> to_list) in
    let ss = fst (List.split ss) in
    List.for_all (fun s -> String.length s > ofs) ss &&
    let c = (List.hd ss).[ofs] in
    List.for_all (fun s -> s.[ofs] = c) ss &&
    List.(length (filter (fun s -> String.length s = ofs + 1) ss) <= 1)
  in
  let rec process_level ofs start xstop =
    assert (invariant ofs start xstop);

    let rec aux current i =
      if i >= xstop then
        current
      else
        let s, v = bs.(i) in
        let suffix = Suffix.of_string_suffix ofs s in
        if String.length s < ofs then
          aux current (i + 1)
        else match current with
          | Empty ->
            aux (Leaf (suffix, v)) (i + 1)
          | Leaf (k', v') as t ->
            if Suffix.equal k' suffix then
              t
            else
            if Suffix.is_empty k' then (
              (* because k' is lexicographically smaller than current: *)
              assert (not Suffix.is_empty current);
              aux (Node (Some (k', v'), CharMap.empty)) i
            ) else
              aux (Node (None, CharMap.empty)) (i - 1)
          | Node (mv,






  in
  aux 0 0 (Array.length bs - 1)
